# Comandi Git e le loro Funzionalità

## Manipolazione dei file

### `git init`
Inizializza un nuovo repository Git nella directory corrente.

### `git status`
Mostra lo stato attuale del repository, inclusi i file modificati e quelli pronti per il commit.

### `git config --global user.name "nome user"`
Imposta il nome dell'utente a livello globale per tutti i repository Git.

### `git config --global user.email email`
Imposta l'email dell'utente a livello globale per tutti i repository Git.

### `cat ~/.gitconfig`
Mostra il contenuto del file di configurazione globale di Git.

### `git add nomeFile`
Aggiunge il file specificato all'area di staging (prepara il file per il commit).

### `rm -rf .git`
Rimuove la directory `.git` e quindi distrugge il repository Git corrente.

### `git commit -m "testo che spiega il commit"`
Crea un commit con un messaggio descrittivo.

### `git add *`
Aggiunge tutti i file modificati e nuovi all'area di staging.

### `git restore --staged nomeFile`
Rimuove il file specificato dall'area di staging.

### `git commit --amend`
Modifica l'ultimo commit, consentendo di aggiungere nuovi cambiamenti o modificare il messaggio del commit.

### `git rm 'nomeFile'`
Rimuove il file dal repository e dal file system.

### `git rm --cached nomeFile`
Rimuove il file dal repository ma lo mantiene nel file system locale.

### `.gitignore`
File speciale che specifica i file e le directory da ignorare nel repository Git.

### `git mv nomeFile nuovoNomeFile`
Rinomina o sposta un file e aggiorna il repository di conseguenza.

## Navigare nella history di git

### `git log`
Mostra la cronologia dei commit del repository.

### `git log --oneline`
Mostra la cronologia dei commit con un formato condensato (una riga per commit).

### `git show`
Mostra i dettagli di un commit specifico, inclusi i cambiamenti apportati.

### `git diff`
Mostra le differenze tra i file modificati che non sono ancora stati aggiunti all'area di staging.

### `git diff --stat`
Mostra un riepilogo delle differenze tra i file in termini di numero di righe aggiunte e rimosse.

### `git checkout`
Cambia il branch o ripristina i file nella directory di lavoro.

### `git checkout -b nome`
Crea e passa a un nuovo branch con il nome specificato.

### `git branch -D nomeBranch`
Elimina il branch specificato forzatamente.

### `git merge nomeBranch`
Unisce il branch specificato nel branch corrente.

### `git merge --continue`
Continua un merge che è stato interrotto per risolvere conflitti.

## Altri comandi utili

### `git clone`
Crea una copia di un repository Git remoto nella directory locale.

### `git cherry-pick`
Applica un commit specifico da un branch a un altro.

### `git reset`
Ripristina il repository a uno stato precedente.

### `git pull`
Scarica e integra le modifiche dal repository remoto al branch corrente.

### `git push`
Carica le modifiche dal branch corrente al repository remoto.

### `git fetch`
Scarica le modifiche dal repository remoto ma non le integra automaticamente nel branch corrente.